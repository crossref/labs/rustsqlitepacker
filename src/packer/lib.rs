use chrono::NaiveDate;
use serde_json::Number;
pub fn date_parts_to_date(date_parts: &serde_json::Value) -> NaiveDate {
    if date_parts.is_null() {
        // return a default date of unix time 0
        return NaiveDate::from_ymd_opt(1970, 1, 1).unwrap();
    }

    let default_year = Number::from(1970);
    let default_month = Number::from(1);
    let default_day = Number::from(1);

    let dead_array = Vec::with_capacity(1);
    let dp_array = date_parts[0].as_array().unwrap_or_else(|| &dead_array);

    // check the bounds on date_parts
    if dp_array.len() < 1 {
        // return a default date of unix time 0
        return NaiveDate::from_ymd_opt(1970, 1, 1).unwrap();
    }

    if dp_array.len() < 2 {
        // if we just have a year, return the year and 1st of January
        return NaiveDate::from_ymd_opt(
            dp_array[0]
                .as_number()
                .unwrap_or_else(|| &default_year)
                .as_i64()
                .unwrap_or_else(|| 1970) as i32,
            1,
            1,
        )
        .unwrap();
    }

    if dp_array.len() < 3 {
        // we have a year and a month
        return NaiveDate::from_ymd_opt(
            dp_array[0]
                .as_number()
                .unwrap_or_else(|| &default_year)
                .as_i64()
                .unwrap_or_else(|| 1970) as i32,
            dp_array[1]
                .as_number()
                .unwrap_or_else(|| &default_month)
                .as_i64()
                .unwrap_or_else(|| 1) as u32,
            1,
        )
        .unwrap();
    }

    // we have a year, month and day
    return NaiveDate::from_ymd_opt(
        dp_array[0]
            .as_number()
            .unwrap_or_else(|| &default_year)
            .as_i64()
            .unwrap_or_else(|| 1970) as i32,
        dp_array[1]
            .as_number()
            .unwrap_or_else(|| &default_month)
            .as_i64()
            .unwrap_or_else(|| 1) as u32,
        dp_array[2]
            .as_number()
            .unwrap_or_else(|| &default_day)
            .as_i64()
            .unwrap_or_else(|| 1) as u32,
    )
    .unwrap();
}

#[cfg(test)]
mod tests {
    use chrono::{Datelike, NaiveDate};

    use crate::date_parts_to_date;
    use serde_json::json;

    #[test]
    fn test_date_parts_to_date() {
        let basic_date_json = json!([[2019]]);
        let basic_date_expected = NaiveDate::from_ymd_opt(2019, 1, 1).unwrap();
        let date_result = date_parts_to_date(&basic_date_json);
        assert_eq!(date_result.year(), basic_date_expected.year());

        let month_date_json = json!([[2019, 2]]);
        let month_date_expected = NaiveDate::from_ymd_opt(2019, 2, 1).unwrap();
        let month_result = date_parts_to_date(&month_date_json);
        assert_eq!(month_result.year(), month_date_expected.year());

        let full_date_json = json!([[1986, 5, 26]]);
        let full_date_expected = NaiveDate::from_ymd_opt(1986, 5, 26).unwrap();
        let full_result = date_parts_to_date(&full_date_json);
        assert_eq!(full_result.year(), full_date_expected.year());
    }
}
