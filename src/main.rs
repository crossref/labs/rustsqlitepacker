use std::fs::File;
use std::io::{BufReader, Read};
use std::{fs, io, thread};

use crossbeam_channel::bounded;
use flate2::read::GzDecoder;
use rayon::prelude::*;
use rusqlite::{params, Connection};

use clap::{App, Arg};

use packer::date_parts_to_date;

fn main() -> std::io::Result<()> {
    let matches = App::new("rustsqlitepacker")
        .version("1.0")
        .author("Martin Paul Eve <meve@crossref.org>")
        .about("Packs the Crossref JSON database into SQLite")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input directory to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .help("Sets the output file to use")
                .required(true)
                .index(2),
        )
        .get_matches();

    let input_dir = matches.values_of_lossy("INPUT").unwrap();
    let output_file = matches.values_of_lossy("OUTPUT").unwrap();

    // limit the number of threads we use
    rayon::ThreadPoolBuilder::new()
        .num_threads(4)
        .build_global()
        .unwrap();

    // setup inter-thread communication
    let (s, r) = bounded::<String>(10000);
    let (s_end, r_end) = bounded::<String>(1);

    // setup the SQLite connection
    let conn = Connection::open(&output_file[0]).unwrap();
    setup_database(&conn).unwrap();

    let mut counter = 0;

    // spawn a writer thread
    thread::spawn(move || loop {
        let msg = r.recv().unwrap();
        conn.execute(&msg, params![]).unwrap();

        if msg == "CREATE UNIQUE INDEX doi_index ON works (doi);" {
            flush_database(&conn).unwrap();
            s_end.send("end".to_string()).unwrap();
            break;
        }

        counter += 1;
        if counter >= 100000 {
            flush_database(&conn).unwrap();
            counter = 0;
        }
    });

    let paths: Vec<_> = fs::read_dir(&input_dir[0])
        .unwrap()
        .map(|r| r.unwrap())
        .collect();

    paths.into_par_iter().for_each(|path| {
        let db = Connection::open_in_memory().unwrap();
        setup_database(&db).unwrap();

        println!("Processing: {:?}", path.path());

        // read file into a vector
        let f = File::open(path.path()).unwrap();
        let mut reader = BufReader::new(f);
        let mut data = Vec::new();
        let reader_result = reader.read_to_end(&mut data);
        reader_result.ok();

        // decompress the .gz
        let str_json = decode_reader(&data).unwrap();

        // load into a JSON object
        let json: serde_json::Value =
            serde_json::from_str(&str_json).expect("JSON was not well-formatted");

        // iterate over JSON object
        if let Some(items) = json["items"].as_array() {
            for item in items {
                let process_result = process_item(&item, &db).unwrap();
                s.send(process_result).unwrap();
            }
        }
    });

    println!("Reached the end of the file.");

    println!("Creating index on DOI field.");

    let index = "CREATE UNIQUE INDEX doi_index ON works (doi);".to_string();
    s.send(index).unwrap();

    loop {
        let msg = r_end.recv().unwrap();
        if msg == "end" {
            break;
        }
    }

    println!("Done.");

    return Ok(());
}

fn process_item(item: &serde_json::Value, db: &Connection) -> Result<String, String> {
    // working fetches
    // get the DOI
    let doi = item["DOI"].as_str().unwrap();

    // get the DOI's URL
    let url = item["URL"].as_str().unwrap_or_else(|| "");

    // get the member
    let member = item["member"].as_str().unwrap_or_else(|| "");

    // get the prefix
    let prefix = item["prefix"].as_str().unwrap_or_else(|| "");

    // get the type
    let type_ = item["type"].as_str().unwrap_or_else(|| "");

    // println!("{:?}", source);

    // get the various dates in the item object
    let created = date_parts_to_date(&item["created"]["date-parts"]);
    let deposited = date_parts_to_date(&item["deposited"]["date-parts"]);

    let mut prepared = db.prepare("INSERT INTO works (doi,url,member,prefix,type,created,deposited,metadata) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)").unwrap();

    prepared.raw_bind_parameter(1, doi).unwrap();
    prepared.raw_bind_parameter(2, url).unwrap();
    prepared.raw_bind_parameter(3, member).unwrap();
    prepared.raw_bind_parameter(4, prefix).unwrap();
    prepared.raw_bind_parameter(5, type_).unwrap();
    prepared.raw_bind_parameter(6, created).unwrap();
    prepared.raw_bind_parameter(7, deposited).unwrap();
    prepared.raw_bind_parameter(8, item.to_string()).unwrap();

    return Ok(prepared.expanded_sql().unwrap());
}

/// # Summary
/// Decode a .gz file into a string
/// # Arguments
///
/// * `bytes`: The binary bytes to decode
///
/// returns: Result<(String), Error>
///
fn decode_reader(bytes: &Vec<u8>) -> io::Result<String> {
    // decode the .gz
    let mut gz = GzDecoder::new(&bytes[..]);
    let mut s = String::new();
    gz.read_to_string(&mut s)?;
    Ok(s)
}

fn flush_database(conn: &Connection) -> rusqlite::Result<()> {
    conn.execute_batch("PRAGMA wal_checkpoint(RESTART);")?;
    Ok(())
}

/// # Summary
/// Build the SQLite database and set optimization parameters
/// # Arguments
///
/// * `conn`: A database connection
///
/// returns: Result<(), Error>
///
fn setup_database(conn: &Connection) -> rusqlite::Result<()> {
    // create the table
    conn.execute_batch(
        "PRAGMA journal_mode = 'WAL';
              PRAGMA synchronous = 0;
              PRAGMA cache_size = 1000000;
              PRAGMA locking_mode = EXCLUSIVE;
              PRAGMA temp_store = MEMORY;",
    )
    .expect("PRAGMA");

    let create_result = conn.execute("CREATE TABLE IF NOT EXISTS 'works' (\'id\' INTEGER PRIMARY KEY AUTOINCREMENT, 'doi' TEXT NOT NULL, 'url' varchar(200) NOT NULL, 'member' varchar(255) NULL, 'created' date NULL, 'prefix' varchar(255) NULL, 'type' text NULL, 'deposited' date NULL, 'metadata' TEXT NULL);", []);
    create_result.ok();

    Ok(())
}
