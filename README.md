# Crossref Data Dump Repacker: Rust Edition
A Rust application that allows you to repack the Crossref data dump into SQLite.

![license](https://img.shields.io/gitlab/license/crossref/labs/rustsqlitepacker) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/rustsqlitepacker)

![Rust](https://img.shields.io/badge/rust-%23092E20.svg?style=for-the-badge&logo=rust&logoColor=white) ![SQLite](https://img.shields.io/badge/SQLite-FCC624?style=for-the-badge&logo=sqlite&logoColor=black)

    USAGE:
        rustsqlitepacker <INPUT> <OUTPUT>

Where `<INPUT>` is the path to the annual [Crossref public data file](https://www.crossref.org/blog/2023-public-data-file-now-available-with-new-and-improved-retrieval-options/) and `<OUTPUT>` is the path to the SQLite database to be created.

The resulting database file will be large; approximately 900GB. The process can take a long time to run, too, as there is a lot of input data to countenance. Creating the index - the last step - took 1 hour and 30 minutes on our test machine. 